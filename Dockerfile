FROM ruby:3.3-alpine



#
# Build Args
#

ARG APP_DIR="/opt/app"



#
# Env
#

ENV \
  APP_SERVER_BIND="0.0.0.0" \
  APP_SERVER_PORT="3000" \
  APP_SERVER_HOSTNAME="localhost" \
  APP_PERFORM_DB_MIGRATE="true" \
  APP_PERFORM_DB_SEED="true" \
  APP_GIT_SYNC_CRON_ENABLED="false" \
  APP_GIT_SYNC_CRON_SCHEDULE="0 */1 * * *" \
  APP_GIT_SYNC_REPOSITORIES="https://bherring@bitbucket.org/bherring/discdb-demo-data.git|."
  


#
# Install Packages
#

RUN echo "Installing buildtime packages..." \
  && apk -v --no-cache add ruby-dev build-base postgresql-dev openssl \
  && echo "Installing runtime packages..." \
  && apk -v --no-cache add git nodejs npm tzdata shared-mime-info



#
# Install Application
#

RUN mkdir -p ${APP_DIR}
ADD src/Gemfile ${APP_DIR}/Gemfile
WORKDIR ${APP_DIR}

RUN echo "Installing Gems" \
  && bundle install

RUN rm -rf ${APP_DIR}
COPY src ${APP_DIR}
RUN bundle install
COPY ./scripts /scripts



#
# Start Application
#

EXPOSE ${APP_PORT}
CMD ["/scripts/start.sh"]