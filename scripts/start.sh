#!/bin/sh

if [[ $(echo "$APP_PERFORM_DB_MIGRATE" | tr '[:upper:]' '[:lower:]') = "true" ]]
then
  echo "DB -> Migrating database"
  rake db:migrate
fi

if [[ $(echo "$APP_PERFORM_DB_SEED" | tr '[:upper:]' '[:lower:]') = "true" ]]
then
  echo "DB -> Seeding database"
  rake db:seed
fi

if [[ $(echo "$APP_GIT_SYNC_CRON_ENABLED" | tr '[:upper:]' '[:lower:]') = "true" ]]
then
  echo "Git Sync -> Enabling cron job"
  whenever --update-crontab
fi

echo "Server -> Starting on ${APP_SERVER_BIND}:${APP_SERVER_PORT}"
rails server -b ${APP_SERVER_BIND} -p ${APP_SERVER_PORT}
