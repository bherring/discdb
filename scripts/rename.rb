require 'rest-client'

API_HOST = 'https://discdb/api'
API_KEY = ''


def get_disc(disc_label)	
    # Get Discs
	json = RestClient.get "#{API_HOST}/discs", {:Authorization => "Token token=#{API_KEY}", :params => {:disc_label => disc_label}}
	
	unless json
		puts "No discs found for #{disc_label}"
		return
	end
	
    discs = JSON.parse(json)
	
	if discs.count > 1
		# Prompt for input
		puts "Multiple Discs Found, please choose which disc to use."
		
		discs.each_with_index do |disc, index|
			puts "#{index + 1}. #{disc['tv_show']} - Season #{disc['season']}"
		end
		
		puts "Enter Disc #: "
		index = ($stdin.gets.chomp.to_i)-1
		
		disc = discs[index]
	else
		disc = discs[0]
	end
end
def get_episode(disc, order)
    (disc['episode_discs'].select { |episode_disc| episode_disc['disc_order'] == order })[0]['episode']
end

def get_episode_file_name(episode, episode_file_name)
    episode_number = episode['number'].to_i < 10 ? "0#{episode['number']}" : episode['number']

	"#{episode['full_name']}#{File.extname(episode_file_name)}"
end

Dir.foreach(ARGV[0]) do |disc|
    next if disc == '.' or disc == '..'
    puts disc
    puts '------------------------------------------'
	
	api_disc = get_disc(disc)
	
    disc_order = 1
    Dir.foreach(File.join(ARGV[0], disc)) do |episode|
        next if episode == '.' or episode == '..'
        eps = get_episode(api_disc, disc_order)
		next unless eps
		
		new_name = get_episode_file_name(eps, episode).gsub(/[\x00\/\\:\*\?\"<>\|]/, '')        
		
        puts "Original file name:      #{episode}"
        puts "New Name:                #{new_name}"
		
		File.rename(File.join(ARGV[0], disc, episode), File.join(ARGV[0], disc, new_name))
        
        disc_order = disc_order+1
    end
    puts ''
    puts ''
    puts ''
end
