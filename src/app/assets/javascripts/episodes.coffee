# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  if $('#episodes_table').length
    $('#episodes_table').dataTable().fnSort([ [3,'asc'], [0,'asc']])