# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  if $('#episode_discs_table').length
    $('#episode_discs_table').dataTable().fnSort([ [4,'asc'], [1,'asc']])