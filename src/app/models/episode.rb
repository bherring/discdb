class Episode < ActiveRecord::Base
  include Filterable

  audited

  belongs_to :user
  belongs_to :season
  belongs_to :tv_show

  has_many :episode_discs, :dependent => :destroy
  has_many :discs, :through => :episode_discs

  accepts_nested_attributes_for :episode_discs


  validates :name, presence: true
  validates :number, presence: true
  validates :season_id, presence: true
  validates :tv_show_id, presence: true

  validates_uniqueness_of :name, scope: [:tv_show_id, :season_id]
  validates_uniqueness_of :number, scope: [:tv_show_id, :season_id]


  scope :starts_with, -> (name) { where('name like ?', "#{name}%")}
  scope :tv_show, -> (tv_show) { where('tv_show_id = ?', TvShow.find_by_name(tv_show).id) }


  def to_s
    "#{self.number.to_s} - #{self.name}"
  end

  def to_hash
    {
        name: self.name,
        number: self.number.is_a?(Array) ? self.number.join(',') : self.number
    }
  end
end
