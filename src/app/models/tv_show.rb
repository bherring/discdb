class TvShow < ActiveRecord::Base
  include Filterable
  extend FriendlyId
  friendly_id :name, use: :slugged

  audited

  belongs_to :user

  has_many :seasons, dependent: :destroy
  has_many :episodes
  has_many :discs
  has_many :collections, :dependent => :destroy

  validates :name, presence: true, uniqueness: true


  scope :starts_with, -> (name) { where('name like ?', "#{name}%")}


  IMPORT_FORMATS = [
      {
          :extentions => %w(yml yaml),
          :mime_type  => 'application/x-yaml',
          :lambda     => lambda { |file|
            YAML.load_file(file.to_s)
          }
      }
  ]

  def to_s
    self.name
  end

  def to_hash
    {
        name: self.name,
        tvdb_id: self.tvdb_id,
        language: self.language,
        seasons: self.seasons.map { |season| season.to_hash },
        collections: self.collections.map { |collection| collection.to_hash },
    }
  end

  def to_yaml_file
    file_name = File.join(EXPORTS_DIRECTORY, "#{SecureRandom.hex}_tv_show_#{self.name}.yml")

    File.open(file_name, 'w') {|f| f.write self.to_hash.to_yaml }

    file_name
  end

  def self.file_to_hash(file, format = nil)
    format ||= File.extname(file).split('.').last

    format_hash = self.get_format(format)

    if format_hash
      hash = format_hash[:lambda].call(file)
    else
      raise DiscDBExceptions::IO::UnsupportedFileFormat.new "Unsupported file format: #{format.to_s}"
    end

    hash
  end

  def self.import_from_file(file, user = User.last.id, format = nil)
    TvShow.import(TvShow.file_to_hash(file, format), user)
  end

  def self.import(tv_show_hash, user = User.last.id)
    ActiveRecord::Base.transaction do
      tv_show_hash = symbolize_keys(tv_show_hash)

      tv_show = TvShow.find_or_create_by(
          :name     => tv_show_hash[:name].lstrip.rstrip,
          :tvdb_id  => tv_show_hash[:tvdb_id],
          :language => tv_show_hash[:language]
      )
      tv_show.user = user
      tv_show.save!


      if tv_show_hash[:collections]
        tv_show_hash[:collections].each do |collection|
          collection_hash = symbolize_keys(collection)
          col = Collection.find_or_create_by!(
              :name         => collection_hash[:name].lstrip.rstrip,
              :tv_show_id   => tv_show.id,
          )
          col.description = collection_hash[:description]
          col.region_code = collection_hash[:region_code]
          col.region_name = collection_hash[:region_name]
          col.user = user
          col.save!
        end
      end

      tv_show_hash[:seasons].each do |current_season_hash|
        next unless current_season_hash
        season_hash = symbolize_keys(current_season_hash)

        season = tv_show.seasons.find_or_create_by!(
            :number   => season_hash[:number],
            :tvdb_id  => season_hash[:tvdb_id]
        )
        season.user = user
        season.save!

        episode_number = 1
        season_hash[:discs].each do |current_disc_hash|
          next unless current_disc_hash
          disc_hash = symbolize_keys(current_disc_hash)

          disc = season.discs.find_or_create_by!(
              :disc_number  => disc_hash[:disc][:disc_number],
              :tv_show      => tv_show,
              :disc_type    => disc_hash[:disc][:disc_type],
              :disc_label   => disc_hash[:disc][:disc_label]
          )
          disc.user = user
          disc.save!


          if disc_hash[:disc][:collection]
            dc = DiscCollection.find_or_create_by!(
                :collection => Collection.find_by_tv_show_id_and_name!(tv_show.id, disc_hash[:disc][:collection]),
                :disc       => disc
            )

            dc.user = user
            dc.save!
          end

          disc_hash[:episodes].each do |current_episode_hash|
            next unless current_episode_hash
            episode_hash = symbolize_keys(current_episode_hash)

            episode_resource = disc.episodes.find_or_create_by!(
                :number             => episode_hash[:number].to_i,
                :tv_show            => tv_show,
                :season             => season,
                :name               => episode_hash[:name].lstrip.rstrip,
            )
            episode_resource.makemkv_name = episode_hash[:makemkv_name] ? episode_hash[:makemkv_name].lstrip.rstrip : nil
            episode_resource.user = user
            episode_resource.save!

            if episode_hash[:order]
              ed = EpisodeDisc.find_or_create_by!(
                  :episode    => episode_resource,
                  :disc       => disc
              )

              ed.disc_order = episode_hash[:order]
              ed.user       = user
              ed.save!

              episode_discs = EpisodeDisc.where(:disc_id => disc.id, :disc_order => episode_hash[:order])
              if episode_discs.count > 1
                # Has Combined Episodes
                episode_discs.each do |episode_disc|
                  episode_disc.combined_episodes = true
                  episode_disc.save!
                end
              end
            end

            episode_number = episode_number+1
          end
        end
      end
    end
  end

  def self.symbolize_keys(hash)
    hash.inject({}){|result, (key, value)|
      new_key = case key
                  when String then key.to_sym
                  else key
                end
      new_value = case value
                    when Hash then symbolize_keys(value)
                    else value
                  end
      result[new_key] = new_value
      result
    }
  end

  private
  def self.get_format(format_extension)
    format = nil

    IMPORT_FORMATS.each do |import_format|
      format = import_format if import_format[:extentions].include?(format_extension.to_s)
    end

    format
  end
end
