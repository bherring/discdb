class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    user.roles << :user unless user.roles.count > 0

    base_models = [TvShow, Season, Disc, Episode, EpisodeDisc, Collection]
    alias_action :create, :read, :update, :to => :cru

    if user.has_role?(:admin)
      can :manage, :all

    elsif user.has_role?(:author)
      can :cru, base_models

    elsif user.has_role?(:user)
      can :read, base_models
      can :export, TvShow
    end

    if user.has_role?(:user)
      can :manage, User
      can :manage, APIKey
    end
  end
end
