class Disc < ActiveRecord::Base
  include Filterable

  audited

  belongs_to :user
  belongs_to :tv_show
  belongs_to :season

  has_many :episode_discs, :dependent => :destroy
  has_many :episodes, :through => :episode_discs
  has_one :disc_collection, :dependent => :destroy
  has_one :collection, :through => :disc_collection


  validates :disc_number, presence: true
  validates :disc_type, presence: true
  validates :tv_show_id, presence: true
  validates :season_id, presence: true

  validates_uniqueness_of :disc_number, scope: [:tv_show_id, :season_id, :disc_type]


  scope :disc_label, -> (disc_label) { where('disc_label = ?', disc_label) }


  def to_s
    str = "#{self.disc_type} - #{self.disc_number}"
    if self.disc_label
      str = "#{str} - #{self.disc_label}"
    end

    str
  end

  def to_hash
    episode_array = Array.new
    self.episode_discs.each do |episode_disc|
      episode_array << episode_disc.episode.to_hash.merge(order: episode_disc.disc_order)
    end

    {
        episodes: episode_array,
        disc: {
            disc_number: self.disc_number,
            disc_type: self.disc_type,
            disc_label: self.disc_label,
            collection: self.collection.name,
        }
    }
  end
end
