class Collection < ActiveRecord::Base
  include Filterable
  extend FriendlyId
  friendly_id :name, use: :slugged

  audited

  has_many :disc_collection, :dependent => :destroy
  has_many :discs, through: :disc_collection

  belongs_to :tv_show
  belongs_to :user

  validates :name, presence: true,
                   uniqueness: true

  def to_s
    self.name
  end

  def to_hash
    {
        name: self.name,
        description: self.description,
        region_code: self.region_code,
        region_name: self.region_name,
    }
  end

  def to_param
    self.name
  end
end