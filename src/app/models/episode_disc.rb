class EpisodeDisc < ActiveRecord::Base
  audited

  belongs_to :episode
  belongs_to :disc
  belongs_to :user

  validates :episode_id, presence: true
  validates :disc_id, presence: true

  validates_uniqueness_of :episode_id, :scope => [:episode_id, :disc_id]


  private
  def parse_episode
    self.combined_episodes = self.combined_episodes.split(',').flatten if self.combined_episodes.is_a?(String)
  end
end
