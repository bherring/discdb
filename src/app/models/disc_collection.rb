class DiscCollection < ActiveRecord::Base
  audited

  belongs_to :collection
  belongs_to :disc
  belongs_to :user
end
