json.array!(tv_shows) do |tv_show|
    json.partial! "api/#{self.controller_path.split('/').second}/tv_shows/tv_show", tv_show: tv_show
    json.url api_tv_show_url(tv_show, format: :json)
end