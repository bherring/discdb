json.array!(episode_discs) do |episode_disc|
  json.partial! "api/#{self.controller_path.split('/').second}/episode_discs/episode_disc", episode_disc: episode_disc, combined_episodes: combined_episodes(episode_disc)
  json.url api_tv_show_season_episode_episode_disc_url(episode_disc.episode.tv_show, episode_disc.episode.season, episode_disc.episode, episode_disc, format: :json)
end