json.extract! episode_disc, :id, :disc_order, :combined_episodes, :created_at, :updated_at

if combined_episodes
  json.combined_episodes do
    json.partial! "api/#{self.controller_path.split('/').second}/episodes/episodes", episodes: combined_episodes
  end
end

json.episode do
  json.partial! "api/#{self.controller_path.split('/').second}/episodes/episode", episode: episode_disc.episode
end

json.disc do
  json.partial! "api/#{self.controller_path.split('/').second}/discs/disc", disc: episode_disc.disc
end
