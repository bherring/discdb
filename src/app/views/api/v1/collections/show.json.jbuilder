json.partial! "api/#{self.controller_path.split('/').second}/collections/collection", collection_resource: @collection
json.url api_tv_show_collection_url(@collection.tv_show, @collection, format: :json)

json.discs do
  json.partial! "api/#{self.controller_path.split('/').second}/discs/discs", discs: @collection.discs
end