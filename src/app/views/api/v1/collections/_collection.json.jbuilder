json.extract! collection_resource, :id, :name, :description, :region_code, :region_name, :tv_show_id, :created_at, :updated_at
json.tv_show collection_resource.tv_show.name