json.array!(seasons) do |season|
    json.partial! "api/#{self.controller_path.split('/').second}/seasons/season", season: season
    json.url api_tv_show_season_url(season.tv_show, season, format: :json)
end