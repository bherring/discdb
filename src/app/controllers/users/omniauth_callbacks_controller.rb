class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def auth_man
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env['omniauth.auth'])

    if @user.persisted?
      session['devise_omniauth_myaccount'] = request.env['omniauth.auth'].info.my_account
      session['devise_omniauth_enable_gravatar'] = request.env['omniauth.auth'].info.enable_gravatar
      session['devise_login_type'] = :omniauth

      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => 'AuthMan') if is_navigational_format?
    else
      raise Exception.new('Failed to create or find user.')
    end
  end
end