class EpisodeDiscsController < ApplicationController
  load_and_authorize_resource
  before_action :set_episode_disc, only: [:show, :edit, :update, :destroy]
  before_action :set_episode, only: :new
  before_action :set_season, only: [:new, :edit, :create, :update]
  before_action :combined_episodes, only: :show

  # GET /episode_discs
  def index
    if params[:disc_id]
      @episode_discs = EpisodeDisc.where(disc_id: params[:disc_id]).all
    elsif params[:episode_id]
      @episode_discs = EpisodeDisc.where(episode_id: params[:episode_id]).all
    else
      @episode_discs = []
    end
  end

  # GET /episode_discs/1
  def show
  end

  # GET /episode_discs/new
  def new
    @episode_disc = @episode.episode_discs.new(
      :disc_id => params[:disc_id]
    )
  end

  # GET /episode_discs/1/edit
  def edit
  end

  # POST /episode_discs
  def create
    @episode_disc = EpisodeDisc.new(episode_disc_params)

    respond_to do |format|
      if @episode_disc.save
        format.html { redirect_to tv_show_season_episode_episode_disc_path(@episode_disc.episode.tv_show, @episode_disc.episode.season, @episode_disc.episode, @episode_disc), notice: 'Episode disc was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /episode_discs/1
  def update
    respond_to do |format|
      if @episode_disc.update(episode_disc_params)
        format.html { redirect_to tv_show_season_episode_episode_disc_path(@episode_disc.episode.tv_show, @episode_disc.episode.season, @episode_disc.episode, @episode_disc), notice: 'Episode disc was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /episode_discs/1
  def destroy
    @episode_disc.destroy
    respond_to do |format|
      format.html { redirect_to tv_show_season_episode_episode_discs_path(@episode_disc.episode.tv_show, @episode_disc.episode.season, @episode_disc.episode), notice: 'Episode disc was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_episode_disc
      @episode_disc = EpisodeDisc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def episode_disc_params
      params.require(:episode_disc).permit(:episode_id, :disc_id, :disc_order)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_episode
      @episode = Episode.find(params[:episode_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_season
      @season = Season.find_by_tv_show_id_and_number(TvShow.friendly.find(params[:tv_show_id]), params[:season_id])
    end

    def combined_episodes
      @combined_episodes = EpisodeDisc.where(disc_id: @episode_disc.disc.id, disc_order: @episode_disc.disc_order).map { |ed| ed.episode } if @episode_disc.combined_episodes?
    end
end
