module EpisodesHelper
  def episode_number(episode)
    episode.number.is_a?(Array) ? episode.number.join(',') : episode.number
  end
end
