Rails.application.routes.draw do
  resources :tv_shows do
    member do
      get :export, :defaults => { :format => :yaml }
    end

    collection do
      get :import
      post :perform_import
    end

    resources :episodes, only: [:index, :show]
    resources :seasons do
      resources :discs do
        resources :episodes, only: [:index, :show]
      end
      resources :episodes do
        resources :episode_discs
      end
    end

    resources :collections do
      resources :discs, only: [:index, :show]

      member do
        get :associate_disc
        post :perform_associate_disc
      end
    end
  end

  root :to => 'tv_shows#index'

  devise_for :users, :controllers => { :omniauth_callbacks => 'users/omniauth_callbacks' }

  resource :users, only: [:show] do
    resources :api_keys
  end

  namespace :admin do
    get '/' => 'admin#index'
    resources :users
  end

  namespace :api, :defaults => {:format => :html} do
    root :to => 'api#index'

    resources :api, :controller => :api, only: [:index, :show]

    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true), :defaults => {:format => :json} do
      resources :tv_shows do
        collection do
          get :import
        end

        resources :episodes, only: [:index, :show]
        resources :seasons do
          resources :discs do
            resources :episodes, only: [:index, :show]
          end
          resources :episodes do
            resources :episode_discs
          end
        end

        resources :collections do
          resources :discs, only: [:index, :show]

          member do
            post :associate_disc
          end
        end
      end
      resources :discs, only: [:index, :show]
    end
  end
end
