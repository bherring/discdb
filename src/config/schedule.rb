# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
#
set :output, "/var/logs/cron_log.log"

set :environment, (ENV['RAILS_ENV'] || 'production')

if (ENV['APP_GIT_SYNC_CRON_ENABLED'] || false).to_s.downcase == "true"
  every ENV['APP_GIT_SYNC_CRON_SCHEDULE'] do
    (ENV['APP_GIT_SYNC_REPOSITORIES'] || []).strip.split(',').each do |repository_info|
      repository_info_arr = repository_info.split('|')
      rake "tv_shows:import_from_git[#{repository_info_arr[0]},#{repository_info_arr[1]}]"
    end
  end
end
