class CreateDiscCollections < ActiveRecord::Migration[4.2]
  def change
    create_table :disc_collections do |t|
      t.integer :disc_id
      t.integer :collection_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
