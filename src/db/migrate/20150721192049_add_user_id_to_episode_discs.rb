class AddUserIdToEpisodeDiscs < ActiveRecord::Migration[4.2]
  def change
    add_column :episode_discs, :user_id, :integer
  end
end
