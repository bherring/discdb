class CreateDiscs < ActiveRecord::Migration[4.2]
  def change
    create_table :discs do |t|
      t.string :disc_number
      t.string :disc_type
      t.string :disc_label
      t.integer :tv_show_id
      t.integer :user_id
      t.integer :season_id

      t.timestamps null: false
    end
  end
end
