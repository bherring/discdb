class AddTvdbIdToSeasons < ActiveRecord::Migration[4.2]
  def change
    add_column :seasons, :tvdb_id, :string
  end
end
