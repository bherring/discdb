class CreateEpisodes < ActiveRecord::Migration[4.2]
  def change
    create_table :episodes do |t|
      t.string :name
      t.integer :number
      t.integer :season_id
      t.integer :user_id
      t.integer :tv_show_id

      t.timestamps null: false
    end
  end
end
