class CreateSeasons < ActiveRecord::Migration[4.2]
  def change
    create_table :seasons do |t|
      t.integer :number
      t.integer :tv_show_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
