class AddCombinedEpisodesToEpisodeDiscs < ActiveRecord::Migration[4.2]
  def change
    add_column :episode_discs, :combined_episodes, :boolean, :default => false
  end
end
