require 'test_helper'

class EpisodeDiscsControllerTest < ActionController::TestCase
  setup do
    @episode_disc = episode_discs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:episode_discs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create episode_disc" do
    assert_difference('EpisodeDisc.count') do
      post :create, episode_disc: { disc_id: @episode_disc.disc_id, disc_order: @episode_disc.disc_order, episode_id: @episode_disc.episode_id }
    end

    assert_redirected_to episode_disc_path(assigns(:episode_disc))
  end

  test "should show episode_disc" do
    get :show, id: @episode_disc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @episode_disc
    assert_response :success
  end

  test "should update episode_disc" do
    patch :update, id: @episode_disc, episode_disc: { disc_id: @episode_disc.disc_id, disc_order: @episode_disc.disc_order, episode_id: @episode_disc.episode_id }
    assert_redirected_to episode_disc_path(assigns(:episode_disc))
  end

  test "should destroy episode_disc" do
    assert_difference('EpisodeDisc.count', -1) do
      delete :destroy, id: @episode_disc
    end

    assert_redirected_to episode_discs_path
  end
end
