namespace :tv_shows do
  Rails.logger = Logger.new(STDOUT)

  def get_system_user()
    app_config_user = AppConfig.get(%w(system_user), APP_CONFIG)
    Rails.logger.debug "Task tv_shows -> get_system_user() -> app_config_user: " << String(app_config_user)

    user = User.find_by_email(app_config_user['email'])
    Rails.logger.debug "Task tv_shows -> get_system_user() -> user: " << String(user)

    return user
  end

  def import_directory(root_directory)
    # Find all yml/yaml files and import them
    Dir.glob("#{root_directory}/*.{yml,yaml}") do |filename|
      data = YAML.load_file(filename)

      Rails.logger.info "Processing TV Show: " << String(data['name'])
      Rails.logger.info "TvShow.find_by_name(data['name'])" << String(TvShow.find_by_name(data['name']))

      Rails.logger.info "\tImporting TV Show..."
      TvShow.import(data, get_system_user())
    end
  end



  desc 'Import a series from a YAML file.'
  task :import_from_yaml, [:yaml] => [:environment] do |t, args|
    TvShow.import(YAML.load_file(args[:yaml]), get_system_user())
  end

  desc 'Import multiple series from a git repository.'
  task :import_from_git, [:repository, :root_directory] => [:environment] do |t, args|
    # Set a default root_directory
    args.with_defaults(root_directory: '.')

    # Create temporary directory to hold our repository clone
    Dir.mktmpdir do |dir|
      # Clone the repository
      g = Git.clone(args[:repository], File.join(dir, 'import_from_git.git'))

      # Change to the new clone
      g.chdir do
        import_directory(args[:root_directory])
      end
    end
  end
end